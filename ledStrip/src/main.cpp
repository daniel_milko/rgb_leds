#ifndef F_CPU
#define F_CPU 8000000UL // 8 MHz clock speed
#endif

#include <stdint.h>
#include <avr/io.h>
#include <util/delay.h>
#include <../lib/WS2812cpp/cRGB.h>

int main(void)
{



  uint8_t adc_lobyte;
  uint16_t raw_adc; 
  
  uint8_t colorMode = 0;   //0-15 --- COLORS ON REMOTE CONTROLLER

  uint16_t brightness; // BRIGHTNESS IN ROOM
  uint16_t brightnessForColor; //STRIP BRIGHTNESS
  uint8_t redValue, greenValue, blueValue; //VALUES FOR LEDS

  class cRGB led[13];
  ADMUX =
            (0 << ADLAR) |     // left shift result
            (0 << REFS1) |     // Sets ref. voltage to VCC, bit 1
            (0 << REFS0) |     // Sets ref. voltage to VCC, bit 0
            (0 << MUX3)  |     // use ADC2 for input (PB4), MUX bit 3
            (0 << MUX2)  |     // use ADC2 for input (PB4), MUX bit 2
            (1 << MUX1)  |     // use ADC2 for input (PB4), MUX bit 1
            (0 << MUX0);       // use ADC2 for input (PB4), MUX bit 0
  ADCSRA = 
            (1 << ADEN)  |     // Enable ADC 
            (1 << ADPS2) |     // set prescaler to 64, bit 2 
            (1 << ADPS1) |     // set prescaler to 64, bit 1 
            (0 << ADPS0);      // set prescaler to 64, bit 0 
  // DDRB |= (1<<DDB1);

  while(1) //infinite loop
  {

    
    
    ADCSRA |= (1 << ADSC);           // start ADC measurement
    while (ADCSRA & (1 << ADSC) ); // wait till conversion complete 
    adc_lobyte = ADCL;               // get the sample value from ADCL
    raw_adc = ADCH<<8 | adc_lobyte;  // add lobyte and hibyte

    brightness = raw_adc / 2.5;
    if(brightness>255) brightness = 255;
    brightnessForColor = (brightness - 255) * (-1);


    switch (colorMode)
    {
    case 0:
      redValue = 0 + brightnessForColor;
      greenValue = 0;
      blueValue = 0;
      break;
    case 1:
      redValue = 0;
      greenValue = 0 + brightnessForColor;
      blueValue = 0;
      break;
    case 2:
      redValue = 0;
      greenValue = 0;
      blueValue = 0 + brightnessForColor;
      break;
    case 3:
      redValue = 0 + brightnessForColor;
      greenValue = 0 + brightnessForColor;
      blueValue = 0 + brightnessForColor;
      break;
    case 4:
      redValue = 0 + brightnessForColor;
      greenValue = 0 + (brightnessForColor/3);
      blueValue = 0;
      break;
    case 5:
      redValue = 0 + (brightnessForColor/3);
      greenValue = 0 + brightnessForColor;
      blueValue = 0;
      break;
    case 6:
      redValue = 0;
      greenValue = 0 + (brightnessForColor/2);
      blueValue = 0 + brightnessForColor;
      break;
    case 7:
      redValue = 0 + brightnessForColor;
      greenValue = 0 + (brightnessForColor/2.5);
      blueValue = 0;
      break;
    case 8:
      redValue = 0;
      greenValue = 0 + (brightnessForColor/1.5);
      blueValue = 0 + brightnessForColor;
      break;
    case 9:
      redValue = 0 + (brightnessForColor/3);
      greenValue = 0 ;
      blueValue = 0 + brightnessForColor;
      break;
    case 10:
      redValue = 0 + brightnessForColor;
      greenValue = 0 + (brightnessForColor/2);
      blueValue = 0;
      break;
    case 11:
      redValue = 0;
      greenValue = 0 + (brightnessForColor/2);
      blueValue = 0 + brightnessForColor;
      break;
    case 12:
      redValue = 0 + (brightnessForColor/2.5);
      greenValue = 0 ;
      blueValue = 0 + brightnessForColor;
      break;
    case 13:
      redValue = 0 + brightnessForColor;
      greenValue = 0 + (brightnessForColor/1.5);
      blueValue = 0;
      break;
    case 14:
      redValue = 0;
      greenValue = 0 + (brightnessForColor/2.5);
      blueValue = 0 + brightnessForColor;
      break;
    case 15:
      redValue = 0 + (brightnessForColor/2);
      greenValue = 0 ;
      blueValue = 0 + brightnessForColor;
      break;
    default:
      break;
    }

    // PORTB |= (1<<PORTB0);
    // _delay_ms(1000); //1 second delay
    // PORTB &= ~(1<<PORTB0);
    // _delay_ms(1000); //1 second delay

    // for(int i=0; i <13; i++){
    //   led[i].r=255;led[i].g=0;led[i].b=0;
      
    // }
    // ws2812_setleds(led,13);
    for(int i=0; i <13; i++){
      led[i].r=redValue;
      led[i].g=greenValue;
      led[i].b=blueValue;
      
    }
    led->ws2812_setleds(led,13);
        colorMode++;
    colorMode %=16;
    _delay_ms(1000);
  }
}